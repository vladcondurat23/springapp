# Etapa de construire cu Gradle
FROM gradle:6.0.1-jdk11 AS builder
COPY springboot /springboot
WORKDIR /springboot
RUN chmod +x gradlew
RUN ./gradlew build --no-daemon

# Etapa finală rularea aplicației pe OpenJDK
FROM openjdk:11
COPY --from=builder /springboot/build/libs/spring-boot-FiiPractic-1.0.jar /springboot/spring-boot-FiiPractic-1.0.jar
WORKDIR /springboot
CMD ["java", "-jar", "spring-boot-FiiPractic-1.0.jar"]

